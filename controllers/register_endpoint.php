<?php
	session_start();
	require_once "./connection.php";

	$firstName = $_POST["firstName"];
	$lastName = $_POST["lastName"];
	$email = $_POST["email"];
	$password = $_POST["password"];
	$confirmPassword = $_POST["confirmPassword"];

	
	// sanitation
	// validation

	if(empty($firstName) || empty($lastName) || empty($email) || empty($password) || empty($confirmPassword)){

		$_SESSION['error'] = "Please fill up required fields.";
	 	return header("location: {$_SERVER["HTTP_REFERER"]}");
	} 

	if($password !== $confirmPassword){
		$_SESSION['error'] = "Password must match confirm password.";
		return header("location: {$_SERVER["HTTP_REFERER"]}");
	}

	$query = "SELECT * FROM users WHERE email = '{$email}'";
	$response = mysqli_query($conn, $query);

	$row = mysqli_fetch_assoc($response);

	if(!empty($row)){
		$_SESSION['error'] = "Email is already used. Please use another email.";
		return header("location: {$_SERVER["HTTP_REFERER"]}");
	} 


	// saving to database


	$query = "INSERT INTO users 
				(firstname, lastname, email, password)
				VALUES 
				('{$firstName}', '{$lastName}', '{$email}', '{$password}')";

	mysqli_query($conn, $query);

	header('LOCATION: /views/login.php');