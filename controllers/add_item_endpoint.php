<?php
	session_start();

	require_once "./connection.php";

	$name = $_POST['name'];
	$price = $_POST['price'];
	$description = $_POST['description'];
	$category_id = $_POST['category'];
	if(empty($name) || 
		empty($price) || 
		empty($category_id) || 
		empty($_FILES['item_image']['name'])){
		$_SESSION['error'] = "Please Fill-up all required fields.";
		return header("location: {$_SERVER['HTTP_REFERER']}");
	}

	if($price < 0){
		$_SESSION['error'] = "Price must be greater than zero(0).";
		return header("location: {$_SERVER['HTTP_REFERER']}");
	}

	$extension = strtolower(pathinfo($_FILES['item_image']['name'], PATHINFO_EXTENSION));

	// check extension if jpg, png, gif
	if( $extension !== "jpg" && 
		$extension !== "jpeg" && 
		$extension !== "png" && 
		$extension !== "gif"){
		$_SESSION['error'] = "Image must be in 'JPG', 'PNG', or 'GIF' format.";
		return header("location: {$_SERVER['HTTP_REFERER']}");
	}

	$upload_path = "../assets/images/".uniqid().'.'.$extension; 
	move_uploaded_file($_FILES['item_image']['tmp_name'], $upload_path);

	$query = "INSERT INTO items (
				name, 
				price, 
				description, 
				image_path, 
				category_id
			) VALUES (
				'{$name}',
				$price,
				'{$description}',
				'{$upload_path}',
				$category_id
			)";

	mysqli_query($conn, $query);

	header("location: ../views/items.php");
