<?php
require_once "../partials/template.php";

function getContent(){
	require_once "../controllers/connection.php";

	// create query
	$query = "SELECT * FROM categories";
	// execute query
	$response = mysqli_query($conn, $query);
	?>
	<div class="container">
		<h1 class="text-center">Item</h1>
		<?php 
			if(array_key_exists('error', $_SESSION)){
				echo "<div class='alert alert-danger' role='alert'>
					{$_SESSION['error']}
					</div>";

				unset($_SESSION['error']);
			}
		?>
		<form 
			action="/controllers/add_item_endpoint.php" 
			method="POST" 
			enctype="multipart/form-data"
		>
			<label for="">Item Name <span class="text-danger">*</span></label>
			<input type="text" name="name" class="form-control">
			<label for="">Price <span class="text-danger">*</span></label>
			<input type="number" name="price" class="form-control" min="0">
			<label for="">Description</label>
			<textarea name="description"rows="3" class="form-control"></textarea>
			<label for="">Image <span class="text-danger">*</span></label>
			<input type="file" name="item_image" class="form-control">
			<label for="">Category <span class="text-danger">*</span></label>
			<select name="category" id="" class="form-control">
				<option value=""></option>
				<?php 
					while($row = mysqli_fetch_assoc($response)){
						echo "<option value='{$row['id']}'>{$row['name']}</option>";
					}
				?>
			</select>
			<button class="btn btn-primary btn-block mt-2">Save</button>
		</form>
	</div>
	<?
}