<?php

require_once "../partials/template.php";

function getContent(){
	?>
		
		<main>
		<div class="container">
			<h1 class="text-center my-4">Register</h1>
			<?php
			if(isset($_SESSION['error'])){

				echo "<div class='alert alert-danger' role='alert'>
						{$_SESSION['error']}
					</div>";

				unset($_SESSION['error']);
			}

			?>
		<form action="/controllers/register_endpoint.php" method="POST">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<input type="text" class="form-control" name="firstName" id="firstName" placeholder="Enter First Name">
			</div>
			<div class="form-group">
				<label for="lastName">Last Name</label>
				<input type="text" class="form-control" name="lastName" id="lastName" placeholder="Enter Last Name">
			</div>
			<div class="form-group">
				<label for="email">Email address</label>
				<input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			</div>
			<div class="form-group">
				<label for="confirmPassword">Confirm Password</label>
				<input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password">
			</div>
			<button type="submit" class="btn btn-primary">Register</button>
		</form>
	</div>
</main>
	<?php
}