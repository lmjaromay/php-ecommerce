<?php 
	session_start();

?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Navbar</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="#">Catalog <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Cart</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">About Us</a>
				</li>
				
				<li class="nav-item">
					<a class="nav-link" href="/views/item_form.php">Item Form</a>
				</li>
				<?php
					if(array_key_exists('logged_user', $_SESSION)) {
						?>

							<li class="nav-item">
								<a href="#" class="nav-link"> <?php echo $_SESSION['logged_user']['firstname'] . ' ' . $_SESSION['logged_user']['lastname']  ?> </a>
							</li>
							<li class="nav-item">
								<a href="/controllers/logout.php" class="nav-link">Logout</a>
							</li>
						<?php

					} else {
						?>
							<li class="nav-item">
								<a class="nav-link" href="/views/login.php">Login</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/views/register.php">Register</a>
							</li>
						<?php
					}
				?>
			</ul>
		</div>
	</nav>