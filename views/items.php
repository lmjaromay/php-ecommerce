<?php
require_once "../partials/template.php";
function getContent() {
	require_once "../controllers/connection.php";
	//Create Query statement getting the list of items
	$query = "SELECT * FROM items";
 	//Execute query
	$response = mysqli_query($conn, $query);
	?>
	<div class="container">
		<div class="row">
			<?php
			while ($row = mysqli_fetch_assoc($response)){
				?>
				<div class="card col-lg-3 col-md-6 col-sm-12 mx-1">
					<img src="<?php echo $row['image_path'] ?>" class="card-img-top" style="
					width: 150px;
					height: 150px;
					object-fit: cover;
					">
					<div class="card-body">
						<h5 class="card-title"><?php echo $row['name']?></h5>
						<p class="card-text">
							<?php echo $row['price']  ?>
						</p>
						<?php 
						if(array_key_exists('logged_user', $_SESSION) && $_SESSION['logged_user']['type'] === 'admin'){
							?>
							<a href="" class="btn btn-primary">Edit</a>
							<a href="" class="btn btn-danger">Delete</a>
							<?php
						} else {
							?>
							<a href="#" class="btn btn-primary">Add to Cart</a>
							<?php
						}
						?>
					</div>
				</div>

				<?php
			}
			?>
		</div>
	</div>
	<?php
}