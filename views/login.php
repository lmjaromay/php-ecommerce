<?php
require_once "../partials/template.php";


function getContent(){
	require_once "../controllers/connection.php";
	echo $dbhost;
	?>
	<main>
		<div class="container">
		<h1 class="text-center my-4">Login</h1>
		<?php
			if(isset($_SESSION['error'])){

				echo "<div class='alert alert-danger' role='alert'>
						{$_SESSION['error']}
					</div>";

				unset($_SESSION['error']);
			}

			?>
		<form action="/controllers/authenticate.php" method="POST">
			<div class="form-group">
				<label for="email">Email address</label>
				<input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			</div>
			<button type="submit" class="btn btn-primary">Login</button>
		</form>
		</div>
	</main>

	<?php
}
