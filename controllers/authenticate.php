<?php
	session_start();
	require_once "./connection.php";

	$email = $_POST['email'];
	$password = $_POST['password'];


	$query = "SELECT * FROM users WHERE email = '{$email}'";

	$user = mysqli_fetch_assoc(mysqli_query($conn, $query));

	if(empty($user)){
		$_SESSION['error'] = "Email or Password is invalid.";
		return header("location: {$_SERVER['HTTP_REFERER']}");
	}

	if($user['password'] !== $password){
		$_SESSION['error'] = "Email or Password is invalid.";
		return header("location: {$_SERVER['HTTP_REFERER']}");
	}

	$_SESSION['logged_user'] = $user;
  
	header('location: /index.php');